import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDateTime;

@AllArgsConstructor
@Data
public class Certificate {
    private LocalDateTime from;
    private String name;
    private Integer count;
}
