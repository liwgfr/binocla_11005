import java.time.LocalDateTime;

public class Main {
    public static void main(String[] args) throws NoSuchMethodException {
        ObjectsGenerator generator = new ObjectsGenerator();
        User user = generator.generate(User.class, "Sergey", "Shamov", 2, 2.54);
        User user1 = generator.generate(User.class, "Sergey", "Shamov", 2, 2.54);
        Certificate cert = generator.generate(Certificate.class, LocalDateTime.now(), "certi", 23);
        System.out.println(cert);
        System.out.println(user);
        System.out.println(user1);
    }
}
