import lombok.SneakyThrows;

import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.List;

public class ObjectsGenerator {
    @SneakyThrows
    public <T> T generate(Class<T> object, Object... properties) throws NoSuchMethodException {
        Class<?>[] typesOfProperties = getTypesOfProperties(properties);
        Constructor<T> constructor = object.getConstructor(typesOfProperties);
        System.out.println(constructor);
        return constructor.newInstance(properties);
    }
    private Class<?>[] getTypesOfProperties(Object... properties) {
        List<Class<?>> typesOfProperties = new ArrayList<>();
        for (Object prop : properties) {
            typesOfProperties.add(prop.getClass());
        }
        Class<?>[] a = new Class[typesOfProperties.size()];
        typesOfProperties.toArray(a);
        return a;
    }

}
