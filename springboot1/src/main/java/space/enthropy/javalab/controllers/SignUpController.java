package space.enthropy.javalab.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import space.enthropy.javalab.dto.UserForm;
import space.enthropy.javalab.services.SignUpService;

@Controller
public class SignUpController {
    @Autowired
    private SignUpService signUpService;

    @GetMapping("/signup")
    public String getSignupPage() {
        return "sign_up_page";
    }

    @PostMapping("/signup")
    public String signUp(UserForm form) {
        signUpService.signUp(form);
        return "redirect:/users";
    }
}
