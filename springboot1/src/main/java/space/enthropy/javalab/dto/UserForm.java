package space.enthropy.javalab.dto;

import lombok.Data;

@Data
public class UserForm {
    private String email;
    private String password;
}
