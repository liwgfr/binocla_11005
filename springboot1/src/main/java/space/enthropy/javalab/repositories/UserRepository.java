package space.enthropy.javalab.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import space.enthropy.javalab.models.User;

public interface UserRepository extends JpaRepository<User, Long> {

}
