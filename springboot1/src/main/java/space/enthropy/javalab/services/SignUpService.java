package space.enthropy.javalab.services;

import space.enthropy.javalab.dto.UserForm;

public interface SignUpService {
    void signUp(UserForm form);
}
