package space.enthropy.javalab.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import space.enthropy.javalab.dto.UserForm;
import space.enthropy.javalab.models.User;
import space.enthropy.javalab.repositories.UserRepository;

@Component
public class SignUpServiceImpl implements SignUpService {
    @Autowired
    private UserRepository userRepository;

    @Override
    public void signUp(UserForm form) {
        User newUser = User.builder().email(form.getEmail())
                .password(form.getPassword())
                .build();
        userRepository.save(newUser);
    }
}
