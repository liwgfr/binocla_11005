package space.enthropy.javalab.services;

import space.enthropy.javalab.dto.UserDto;

import java.util.List;

public interface UsersService {
    List<UserDto> getAllUsers();
}
