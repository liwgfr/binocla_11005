package space.enthropy.javalab.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import space.enthropy.javalab.dto.UserDto;
import space.enthropy.javalab.models.User;
import space.enthropy.javalab.repositories.UserRepository;

import java.util.List;

@Component
public class UsersServiceImpl implements UsersService {
    @Autowired
    private UserRepository userRepository;
    @Override
    public List<UserDto> getAllUsers() {
        List<User> list = userRepository.findAll();
        return UserDto.from(list);
    }
}
