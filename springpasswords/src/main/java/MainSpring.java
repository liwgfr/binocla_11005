import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import lombok.SneakyThrows;
import org.apache.log4j.PropertyConfigurator;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import javax.sql.DataSource;
import java.io.FileReader;
import java.util.Properties;

/**
 * 04.09.2021
 * 04. Spring Demo
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class MainSpring {
    @SneakyThrows
    public static void main(String[] args) {

        ApplicationContext context = new ClassPathXmlApplicationContext("context.xml");
        SignUpService signUpService = context.getBean(SignUpService.class);
        signUpService.signUp("sidikov.marsel@gmail.com", "qwerty");
        Properties properties = new Properties();
        properties.load(new FileReader("/home/binocla/IdeaProjects/javalab/springpasswords/src/main/resources/application.properties"));


        HikariConfig config = new HikariConfig();
        config.setDriverClassName(properties.getProperty("db.driver"));
        config.setJdbcUrl(properties.getProperty("db.url"));
        config.setUsername(properties.getProperty("db.user"));
        config.setPassword(properties.getProperty("db.password"));

        String log4jConfPath = "/home/binocla/IdeaProjects/javalab/springpasswords/src/main/resources/log4j.properties";
        PropertyConfigurator.configure(log4jConfPath);

    }
}
