import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * 04.09.2021
 * 04. Spring Demo
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class PasswordBlackListHardCodeImpl implements PasswordBlackList {

    private static List<String> blacklist = Arrays.asList("qwerty007", "qwerty008", "qwerty", "marsel");

    @Override
    public boolean contains(String password) {
        return blacklist.contains(password);
    }
}
