import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;

public class PasswordBlackListJDBCImpl implements PasswordBlackList {
    private final JdbcTemplate jdbcTemplate;

    public PasswordBlackListJDBCImpl(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    //language=SQL
    private static final String SQL_SELECT_ALL = "select * from passwords where pass = ?";

    @Override
    public boolean contains(String password) {
        String res = jdbcTemplate.queryForObject(SQL_SELECT_ALL, String.class, password);
        return res != null;


    }
}
